# users_native

Parses the password file `/etc/passwd` to obtain information about users

This crate is available on [crates.io](https://crates.io/crates/users_native) and can
be used by adding the following to your `Cargo.toml` file:

```toml
[dependencies]
users_native = "*"
```
