// Copyright (C) 2015, Alberto Corona <ac@albertocorona.com>
// All rights reserved. This file is part of core-utils, distributed under the
// GPL v3 license. For full terms please see the LICENSE file.

//! This library parses `/etc/passwd` for information about groups on a system
//!
//! Groups are represented in `/etc/passwd` in the form of:
//!
//! `user_name:password:UID:GID:GECOS:home_dir:shell`

use std::io;
use std::fs;
use std::path::{Path};
use std::io::{BufReader,BufRead};
use std::env;

/// Structure used to wrap parsed information from user database
#[derive(Debug,PartialEq,Clone)]
pub struct User {
    /// User's name
    pub name: String,
    /// User's UID
    pub uid: u32,
    /// User's numeric primary group ID
    pub gid: u32,
    /// User's comment field
    pub gecos: Option<String>,
    /// User's home directory
    pub directory: String,
    /// User's shell used at login
    pub shell: String
}

fn parse_line(line: String) -> Option<User> {
    let mut split: Vec<&str> =  line.split(':').collect();
    if split.is_empty() { return None; };
    // Remove 'x' (password)
    split.remove(1);
    let user = User {
        name: String::from(split[0]),
        uid: split[1].parse::<u32>().unwrap(),
        gid: split[2].parse::<u32>().unwrap(),
        gecos:
            if split[3].is_empty() { None }
            else { Some(String::from(split[3])) },
        directory: String::from(split[4]),
        shell: String::from(split[5])
    };
    return Some(user)
}

fn read_passwd() -> Result<Vec<User>, io::Error> {
    let mut users = Vec::new();
    let file = match fs::File::open("/etc/passwd") {
        Ok(s) => { s },
        Err(e) => { return Err(e) }
    };
    let file_buffer = BufReader::new(&file);
    for line in file_buffer.lines() {
        match line {
            Ok(l) => {
                users.push(parse_line(l).unwrap());
            },
            Err(e) => { return Err(e) }
        }

    }
    return Ok(users);
}

fn alt_read_passwd<P: AsRef<Path>>(path: P) -> Result<Vec<User>, io::Error> {
    let mut users = Vec::new();
    let file = match fs::File::open(path) {
        Ok(s) => { s },
        Err(e) => { return Err(e) }
    };
    let file_buffer = BufReader::new(&file);
    for line in file_buffer.lines() {
        match line {
            Ok(l) => {
                users.push(parse_line(l).unwrap());
            },
            Err(e) => { return Err(e) }
        }

    }
    return Ok(users);
}

/// Gets a user by their name
///
/// #Example
///
/// ```
/// use users_native as users;
///
/// let user = users::get_user_by_name("bin").unwrap();
/// assert_eq!(user.name, "bin");
pub fn get_user_by_name(name: &str) -> Option<User> {
    let users = match read_passwd() {
        Ok(s) => { s },
        Err(_) => { return None }
    };
    for user in users {
        if &user.name == name {
            return Some(user);
        }
    }
    return None;
}

/// Gets a user by their UID
///
/// #Example
///
/// ```
/// use users_native as users;
///
/// let user = users::get_user_by_uid(1).unwrap();
/// assert_eq!(user.uid, 1);
pub fn get_user_by_uid(uid: u32) -> Option<User> {
    let users = match read_passwd() {
        Ok(s) => { s },
        Err(_) => { return None }
    };
    for user in users {
        if user.uid == uid {
            return Some(user);
        }
    }
    return None;
}

/// Gets the current user
///
/// **Note**: This uses the "USERNAME" environment variable for obtaining 
/// information on the current user
///
/// #Example
///
/// ```
/// use users_native as users;
///
/// let user = users::get_current_user();
pub fn get_current_user() -> User {
    return get_user_by_name(&get_current_username()).unwrap();
}

/// Gets the current username
///
/// **Note**: This uses the "USERNAME" environment variable for obtaining 
/// information on the current user
///
/// #Example
///
/// ```
/// use users_native as users;
///
/// let user = users::get_current_username();
pub fn get_current_username() -> String {
    let mut user = String::new();
    for (key, value) in env::vars() {
        if key == "USERNAME" { user = value; }
    };
    return user;
}

/// Returns a vector of all users on the system
///
/// #Example
///
/// ```
/// use users_native as users;
///
/// for user in users::get_users().unwrap() {
///     println!("{:?}", user);
/// }
pub fn get_users() -> Option<Vec<User>> {
    match read_passwd() {
        Ok(s) => { return Some(s) },
        Err(_) => { return None }
    }
}

/// Alternative to `get_users`, in the case that the user database. Returns a
/// vector of all users on the system
///
/// #Example
///
/// ```
/// use users_native as users;
///
/// for user in users::get_users().unwrap() {
///     println!("{:?}", user);
/// }
pub fn alt_get_users<P: AsRef<Path>>(path: P) -> Option<Vec<User>> {
    match alt_read_passwd(path) {
        Ok(s) => { return Some(s) },
        Err(_) => { return None }
    }
}

#[test]
fn test_read_passwd() {
    let users = read_passwd().unwrap();
    for user in users.iter() {
        println!("{:?}", user);
    }
    assert_eq!(true, !users.is_empty());
}

#[test]
fn test_alt_read_password() {
    let users = alt_read_passwd("/etc/passwd").unwrap();
    for user in users.iter() {
        println!("{:?}", user);
    }
    assert_eq!(true, !users.is_empty());
}

#[test]
fn test_alt_get_users() {
    let users = alt_get_users("/etc/passwd").unwrap();
    for user in users.iter() {
        println!("{:?}", user);
    }
    assert_eq!(true, !users.is_empty());
}

#[test]
fn test_get_user_by_name() {
    let user = get_user_by_name("bin").unwrap();
    assert_eq!(user.name, "bin");
}

#[test]
fn test_get_user_by_uid() {
    let user = get_user_by_uid(1).unwrap();
    assert_eq!(user.uid, 1);
}

#[test]
fn test_get_current_user() {
    let user_name = get_current_username();
    let test_user = get_current_user();

    assert_eq!(user_name, test_user.name);
}

#[test]
fn test_get_current_username() {
    let mut user_name = String::new();
    for (key, value) in env::vars() {
        if key == "USERNAME" {
            user_name = value;
        }
    }
    let test_user_name = get_current_username();

    assert_eq!(user_name, test_user_name);
}
